//
//  ShakeViewController.swift
//  EnergyWaste
//
//  Created by Nurlan Almazov on 5/22/19.
//  Copyright © 2019 Nurlan Toktonaliev. All rights reserved.
//

import UIKit

class ShakeViewController: UIViewController {
    
    @IBOutlet weak var baseTimerLabel: UILabel!
    @IBOutlet weak var shakeTimerLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    private var shakeVM = ShakeViewModel()
    private var baseTimer: Timer?
    private var shakeTimer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupBindings()
        createBaseTimer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.becomeFirstResponder()
    }
    
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
}

// MARK: - Detecting Motions
extension ShakeViewController {
    
    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        
        if motion == .motionShake {
            self.createShakeTimer()
            self.stopBaseTimer()
            self.shakeVM.isShaking = true
            self.view.backgroundColor = UIColor.yellow
        }
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        
        if motion == .motionShake {
            self.stopShakeTimer()
            self.createBaseTimer()
            self.shakeVM.isShaking = false
            self.view.backgroundColor = UIColor.white
        }
    }
}

// MARK: - Timer
extension ShakeViewController {
    
    func createBaseTimer() {
        
        if self.baseTimer == nil {
            self.baseTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateBaseTimer), userInfo: nil, repeats: true)
        }
    }
    
    func createShakeTimer() {
        
        if self.shakeTimer == nil {
            self.shakeTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateShakeTimer), userInfo: nil, repeats: true)
        }
    }
    
    func stopBaseTimer() {
        
        self.baseTimer?.invalidate()
        self.baseTimer = nil
        self.shakeVM.baseSeconds.value = 0
    }
    
    func stopShakeTimer() {
        
        self.shakeTimer?.invalidate()
        self.shakeTimer = nil
        self.shakeVM.shakeSeconds.value = 0
    }
    
    @objc func updateBaseTimer() {
        
        self.shakeVM.baseSeconds.value += 1
    }
    
    @objc func updateShakeTimer() {
        
        self.shakeVM.shakeSeconds.value += 1
    }
}

// MARK: - Private Methods
private extension ShakeViewController {
    
    func setupBindings() {
        
        self.shakeVM.message.bind {
            self.messageLabel.text = $0.rawValue
        }
        
        self.shakeVM.baseSeconds.bind {
            self.shakeVM.setMessage()
            self.baseTimerLabel.text = "\($0)"
        }
        
        self.shakeVM.shakeSeconds.bind {
            self.shakeVM.setMessage()
            self.shakeTimerLabel.text = "\($0)"
        }
    }
}

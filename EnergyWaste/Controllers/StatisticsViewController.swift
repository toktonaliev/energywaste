//
//  StatisticsViewController.swift
//  EnergyWaste
//
//  Created by Nurlan Almazov on 5/22/19.
//  Copyright © 2019 Nurlan Toktonaliev. All rights reserved.
//

import UIKit

class StatisticsViewController: UIViewController {
    
    @IBOutlet weak var numberOfTriesLabel: UILabel!
    @IBOutlet weak var numberOfSuccessLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        setValues()
    }
    
    @IBAction func close(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    

    @IBAction func reset(_ sender: UIBarButtonItem) {
        
        let alertController = UIAlertController(title: "Вы уверены?", message: "", preferredStyle: .alert)
        let actionYes = UIAlertAction(title: "Да", style: .default) { action in
            
            UserDefaults.standard.set(0, forKey: "numberOfTries")
            UserDefaults.standard.set(0, forKey: "numberOfSuccess")
            
            self.setValues()
        }
        let actionNo = UIAlertAction(title: "Нет", style: .cancel, handler: nil)
        
        alertController.addAction(actionYes)
        alertController.addAction(actionNo)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: Private Methods
private extension StatisticsViewController {
    
    func setValues() {

        if let numberOfTries = UserDefaults.standard.value(forKey: "numberOfTries") as? Int, let numberOfSuccess = UserDefaults.standard.value(forKey: "numberOfSuccess") as? Int {
            
            self.numberOfTriesLabel.text = String(numberOfTries)
            self.numberOfSuccessLabel.text = String(numberOfSuccess)
        } else {
            
            self.numberOfTriesLabel.text = "0"
            self.numberOfSuccessLabel.text = "0"
        }
    }
}

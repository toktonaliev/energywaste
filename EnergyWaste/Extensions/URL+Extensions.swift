//
//  URL+Extensions.swift
//  EnergyWaste
//
//  Created by Nurlan Almazov on 5/22/19.
//  Copyright © 2019 Nurlan Toktonaliev. All rights reserved.
//

import Foundation

extension URL {
    
    static var base: String {
        return "https://betaapi.nasladdin.club/api"
    }
    
    static var result: URL? {
        return URL(string: base + "/energy/operationStatus")
    }
}

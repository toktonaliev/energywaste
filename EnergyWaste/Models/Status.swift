//
//  Status.swift
//  EnergyWaste
//
//  Created by Nurlan Almazov on 5/22/19.
//  Copyright © 2019 Nurlan Toktonaliev. All rights reserved.
//

import Foundation

enum ValidStatus: Int, Codable {
    case successfull = 1
    case unsuccessfull = 0
}

struct Status: Codable {
    let status: Int
}

extension Status {
    
    init(_ status: ValidStatus) {
        self.status = status.rawValue
    }
}

extension Status {
    
    static func create(status: ValidStatus) -> Resource<Status?> {
        
        let status = Status(status)
        
        guard let url = URL.result else {
            fatalError("URL is incorrect!")
        }
        
        guard let data = try? JSONEncoder().encode(status) else {
            fatalError("Error encoding order!")
        }
        
        var resource = Resource<Status?>(url: url)
        resource.httpMethod = .put
        resource.body = data
        
        return resource
    }
}

//
//  ShakeViewModel.swift
//  EnergyWaste
//
//  Created by Nurlan Almazov on 5/22/19.
//  Copyright © 2019 Nurlan Toktonaliev. All rights reserved.
//

import Foundation

enum Message: String {
    case blank = ""
    case request = "Пожалуйста потратьте энергию"
    case inProgress = "Вы тратите энергию"
    case success = "Энергия успешно потрачена"
}

class ShakeViewModel {
    
    var baseSeconds: Box<Int> = Box(0)
    var shakeSeconds: Box<Int> = Box(0)
    
    private var requestSeconds: Int = 0
    
    var isShaking: Bool = false {
        willSet {
            if newValue {
                
                self.numberOfTries += 1
            } else {
                
                self.requestSeconds = 0
                print("NUMBWER OF FAILS: \(self.numberOfFails)")
                if self.numberOfFails > 3 {
                    self.sendRequestToApi(success: false)
                }
                self.numberOfFails += 1
            }
            self.setMessage()
        }
    }
    
    var message: Box<Message> = Box(.request)
    
    private var numberOfTries: Int {
        get {
            if let numberOfTries = UserDefaults.standard.value(forKey: "numberOfTries") as? Int {
                return numberOfTries
            } else {
                return 0
            }
        } set {
            print("NUMBER OF TRIES: \(newValue)")
            UserDefaults.standard.set(newValue, forKey: "numberOfTries")
        }
    }
    
    private var numberOfSuccess: Int {
        get {
            if let numberOfSuccess = UserDefaults.standard.value(forKey: "numberOfSuccess") as? Int {
                return numberOfSuccess
            } else {
                return 0
            }
        } set {
            print("NUMBER OF SUCCESS: \(newValue)")
            UserDefaults.standard.set(newValue, forKey: "numberOfSuccess")
        }
    }
    
    private var numberOfFails: Int = 0
}

extension ShakeViewModel {
    
    func setMessage() {
        
        if self.isShaking {
            
            if (self.shakeSeconds.value > 0 && self.shakeSeconds.value < 4) || (self.shakeSeconds.value > 9 && self.shakeSeconds.value % 10 < 4) {
                
                self.message.value = .inProgress
                
                if self.shakeSeconds.value > 9 {
                    
                    self.message.value = .success
                    
                    if self.shakeSeconds.value % 10 == 0 {
                        
                        self.numberOfFails = 0
                        self.numberOfSuccess += 1
                        sendRequestToApi(success: true)
                    }
                }
            } else {
                
                self.message.value = .blank
            }
        } else {
            
            self.requestSeconds += 1
            if self.requestSeconds >= 5 {
                
                self.message.value = .request
                
                if self.requestSeconds >= 7 {
                    
                    self.message.value = .blank
                    self.requestSeconds = 0
                }
            }
        }
    }
    
    private func sendRequestToApi(success: Bool) {
        
        if success {
            
            APIService().load(resource: Status.create(status: .successfull)) { result in
                
                switch result {
                case .success(let status):
                    print(status)
                case .failure(let error):
                    print(error)
                }
            }
        } else {
            
            APIService().load(resource: Status.create(status: .unsuccessfull)) { result in
                
                switch result {
                case .success(let status):
                    print(status)
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
}

class Box<T> {
    
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ value: T) {
        self.value = value
    }
    
    func bind(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
}

//
//  ShakeViewModelTests.swift
//  EnergyWasteTests
//
//  Created by Nurlan Almazov on 5/23/19.
//  Copyright © 2019 Nurlan Toktonaliev. All rights reserved.
//

import XCTest
@testable import EnergyWaste

class ShakeViewModelTests: XCTestCase {

    private var shakeVM: ShakeViewModel!
    
    override func setUp() {
        super.setUp()
        
        self.shakeVM = ShakeViewModel()
    }
    
    func test_should_make_sure_that_default_message_is_request() {
        
        XCTAssertEqual(self.shakeVM.message.value, .request)
    }
    
    func test_should_make_sure_that_number_of_tries_is_incrementing_after_shake() {
        
        let key = "numberOfTries"
        
        if let prevNumberOfTries = UserDefaults.standard.value(forKey: key) as? Int {
            
            self.shakeVM.isShaking = true
            
            if let numberOfTries = UserDefaults.standard.value(forKey: key) as? Int {
                XCTAssertEqual(prevNumberOfTries + 1, numberOfTries)
            }
        }
    }
}
